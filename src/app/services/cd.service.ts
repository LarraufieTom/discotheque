import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/*
* Application name	PlayerAudio
* API key	ea9621a35c9eb6f7b507539aea5be58f
* Shared secret	0ccf704f993ed48dabe8f0d6cea60677
* Registered to	tlarraufie
*/

@Injectable({
    providedIn: 'root'
})
export class CdService {
    url = 'http://ws.audioscrobbler.com/2.0/?method=album.search&format=json';
    apiKey = 'ea9621a35c9eb6f7b507539aea5be58f'; // <-- Enter your own key here!

    /**
     * Constructor of the Service with Dependency Injection
     * @param http The standard Angular HttpClient to make requests
     */
    constructor(private http: HttpClient) { }

    /**
     * Get data from the radioFmApi
     * map the result to return only the results that we need
     *
     * @param {string} album Search Term
     * @returns Observable with the search results
     */
    searchData(album: string): Observable<any> {

        if (album !== ''){
            return this.http.get(`${this.url}&album=${album}&api_key=${this.apiKey}`).pipe(
                map((results ) => {
                    /*return results['results'];*/
                    return results['results']['albummatches'];
                })

            );
        }
    }
}
