import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

import {Disc} from '../model/disc';

export class DatabaseService {

    protected async getDataTable(key: string) {
        const data = await Storage.get({key});
        return JSON.parse(data.value);
    }

    protected async saveData(key: string, value: object){
        await Storage.set({key, value: JSON.stringify(value)});
    }

    protected async removeData(key: string){
        await Storage.remove({key});
    }


    async getAll(): Promise<Disc[]> {
        console.log('getALL()');
        return await this.getDataTable('albums').then( async tableAlbums => {
                console.log('DB.getAll, tableAlbums ', tableAlbums);
                const results: Disc[] = [];

                for (const data of tableAlbums) {
                    console.log('DB.getAll.forEach, data ', data);
                    // ON AJOUTE LE DISC SI IL EXISTE DANS LA BDD
                    await this.getDataTable(data.toString()).then(disc => {
                        console.log('DB.getAll.forEach, disc ', disc);
                        return results.push(new Disc(disc.id, disc.image, disc.title, disc.artist, disc.genre, disc.titles, disc.year));
                    });
                }
                return results;

        }).catch(async () => {
            console.log('ERREUR CATCH DANS LE getALL()');
            console.log('DB_ERROR');
            return [];
        });
        /*const results: Disc[] = [];
        const result = await Storage.get({key: 'albums'});
        /!*console.log(result);*!/
        console.log(JSON.parse(result.value));
        return JSON.parse(result.value);*/
        return [];
    }

    async addDisc(disc: Disc){
        const discs: number[] = await this.getDataTable('albums').then(res => {
            return (res === null) ? [] : res;
        }).catch( async () => []);
        console.log('addDisc.var discs : ', discs);

        if (discs === null){
            disc.id = 0;

        }else{
            // On récupère le dernier ID de la liste
            const lastId = discs.length - 1;
            // On incrémente l'id + 1 pour le nouveau disque de la liste
            disc.id = lastId + 1;
        }

        discs.push(disc.id);
        await this.saveData(JSON.stringify(disc.id), disc);
        await this.saveData('albums', discs);

        return discs;

    }

    async removeDisc(idDisc: string){
        // On récupère la liste de discs
        const discs: number[] = await this.getDataTable('albums').then(res => {
            return (res === null) ? [] : res;
        });

        console.log('REMOVE METHOD, discs:', discs);

        // On recherche l'index de disc à supprimer
        const index = discs.indexOf(Number(idDisc));
        console.log('index :', index);
        if (index > -1){
            discs.splice(index, 1);
            this.saveData('albums', discs);
        }

        const discss: number[] = await this.getDataTable('albums').then(res => {
            return (res === null) ? [] : res;
        });

        console.log('REMOVE METHOD, discs:', discss);

        /*await this.removeData(idDisc);
        console.log('removeDAta: ', idDisc);*/
    }
}

