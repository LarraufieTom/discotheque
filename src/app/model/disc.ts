export class Disc {
    id: number;
    image: string;
    title: string;
    artist: string;
    genre: string;
    titles: Array<string>;
    year: number;

    constructor(id: number, image: string, title: string, artist: string, genre: string, titles: Array<string>, year: number) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.artist = artist;
        this.genre = genre;
        this.titles = titles;
        this.year = year;
    }
}
