import { Component } from '@angular/core';
import {DatabaseService} from '../app/services/database.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Disc} from './model/disc';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Albums',
      url: '/',
      icon: 'musical-notes'
    },
    {
      title: 'Search',
      url: '/search',
      icon: 'search'
    },
    {
      title: 'Add CD',
      url: '/add-cd',
      icon: 'add'
    },
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
/*    private db: DatabaseService*/
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

}
