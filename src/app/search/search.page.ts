import { Component, OnInit } from '@angular/core';
import {CdService} from '../services/cd.service';
import {Disc} from '../model/disc';
import {DatabaseService} from '../services/database.service';

import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

    results: Array<Disc>;
    album: string = '';
    /**
     * Constructor of our first page
     * @param cdService
     */
    constructor(private cdService: CdService, private db: DatabaseService, private toastController: ToastController) {
        this.results = [];
    }

    ngOnInit() {
    }

    async addCD(disc: Disc){
        this.db.addDisc(disc);

        const toast = await this.toastController.create({
            message: 'Album ajouté dans votre liste !',
            duration: 2000,
            color: 'primary',
            position: 'top'
        });
        toast.present();
    }

    searchChanged() {

        console.log('searchChanged: ERROR');
        // On appel le service qui nous retourne un tableau d'albums
        this.cdService.searchData(this.album).subscribe(
            (result) => {
                this.results = [];
                console.log(result);

                result['album'].forEach(element => {
                        /*console.log('ARTIST '+element['artist'])
                        console.log('IMAGE'+ element['image'][3]['#text']);*/
                        this.results.push(new Disc(0, element['image'][3]['#text'],element['name'],element['artist'],'',[], 2020));
                });
            }
        );
    }
}
