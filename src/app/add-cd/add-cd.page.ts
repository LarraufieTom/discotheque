import { Component, OnInit } from '@angular/core';
import {Disc} from '../model/disc';
import {DatabaseService} from '../services/database.service';
import {Data} from '@angular/router';

import { AlertController, ToastController } from '@ionic/angular';


@Component({
  selector: 'app-add-cd',
  templateUrl: './add-cd.page.html',
  styleUrls: ['./add-cd.page.scss'],
})
export class AddCdPage implements OnInit {

  album = {
    id: 0,
    image: '../../assets/img/default.png',
    title: '',
    artist: '',
    genre: '',
    titles: '',
    year: 2020
  };

  constructor(private db: DatabaseService, private alertController: AlertController, private toastController: ToastController) { }

  async addDiscForm() {
    console.log(this.album);
    if (this.album.title === '' || this.album.artist === '') {

      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Attention',
        subHeader: 'Champs requis',
        message: 'Title et Artist',
        buttons: ['OK']
      });

      await alert.present();

    }else{
      // tslint:disable-next-line:max-line-length
      // @ts-ignore
      await this.db.addDisc(new Disc(this.album.id, this.album.image, this.album.title, this.album.artist, this.album.genre, this.album.titles, this.album.year));

      const toast = await this.toastController.create({
        message: 'Album ajouté dans votre liste !',
        duration: 2000,
        color: 'primary',
        position: 'top'
      });
      toast.present();
    }
  }


  ngOnInit() {
  }

}
