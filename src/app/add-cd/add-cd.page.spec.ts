import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddCdPage } from './add-cd.page';

describe('AddCdPage', () => {
  let component: AddCdPage;
  let fixture: ComponentFixture<AddCdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCdPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddCdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
