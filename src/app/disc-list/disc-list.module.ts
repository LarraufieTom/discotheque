import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import {DiscListComponent} from './disc-list.component';

@NgModule({
    imports: [ CommonModule, FormsModule, IonicModule],
    declarations: [DiscListComponent],
    exports: [DiscListComponent]
})
export class DiscListComponentModule {}
