import { Component, OnInit, Input } from '@angular/core';
import {DatabaseService} from '../services/database.service';
import { Disc } from '../model/disc';


@Component({
    selector: 'app-disc-list-component',
    templateUrl: './disc-list.component.html',
    styleUrls: ['./disc-list.component.scss'],
})
export class DiscListComponent implements OnInit{
    trackVisible = false;
    listCD: Disc[];

    @Input()
    sort: string;


    constructor(private db: DatabaseService) {}

    async getAlbums(){
        /*this.db.addDisc(new Disc(0, 'image', 'title', 'artist', 'genre', ['allo', 'toto'], 2050));*/
        // Récupère tout les objets Disc de la liste
        const list = await this.db.getAll().then(res => {
            console.log('disc-list: variable RES :', res);
            return res;
        });
        return list;
        // On affecte la liste des obets Disc à notre variable CDList
     /*   this.listCD = result;*/

   /*     console.log('tab1.getAlbums, CDList tab1 => : ', this.CDList);*/
    }


    async removeCD(idDisc: number){
        this.db.removeDisc(idDisc.toString());
        location.reload();
    }

    async ngOnInit() {
        /*this.db.removeDisc('albums');*/
        this.listCD = await this.getAlbums();
        console.log('sort', this.sort);
        console.log('listCD', this.listCD);
        console.log('ngOnInit');

        this.listCD.sort((a, b) => {
            console.log('sort', this.sort);
            switch (this.sort) {
                case 'alphabet': {
                    // alphabetical sort
                    if (a.title < b.title) {return -1; }
                    if (a.title > b.title) {return 1; }
                    return 0;
                    break;
                }
                case 'artist': {
                    // artist sort
                    if (a.artist < b.artist) {return -1; }
                    if (a.artist > b.artist) {return 1; }
                    return 0;
                    break;
                }
                case 'genre': {
                    // gender sort
                    if (a.genre < b.genre) {return -1; }
                    if (a.genre > b.genre) {return 1; }
                    return 0;
                    break;
                }
                case 'year': {
                    // year sort
                    if (a.year < b.year) {return -1; }
                    if (a.year > b.year) {return 1; }
                    return 0;
                    break;
                }
            }
        });

    }
}
