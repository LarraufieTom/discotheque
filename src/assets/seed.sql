CREATE TABLE IF NOT EXISTS album(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    image TEXT,
    title TEXT,
    artist TEXT,
    genre TEXT,
    tracks
);
INSERT or IGNORE INTO album VALUES (1, 'afterHours.png', 'After Hours', 'The Weeknd', 'Pop','');
INSERT or IGNORE INTO album VALUES (2, 'maes.jpg', 'Les derniers salopards', 'Maes', 'Rap','');
INSERT or IGNORE INTO album VALUES (3, 'destin.jpg', 'Destin', 'Ninho', 'Rap','');
